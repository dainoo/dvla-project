package com.ernest.genesys.devices.fingerprint;





import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.dermalog.common.DermalogImage;
import com.dermalog.imaging.capturing.Device;
import com.dermalog.imaging.capturing.DeviceManager;
import com.dermalog.imaging.capturing.OnDetectListener;
import com.dermalog.imaging.capturing.OnImageListener;
import com.dermalog.imaging.capturing.cwrap.vc.DeviceInfo;
import com.dermalog.imaging.capturing.exception.ListenerException;
import com.dermalog.imaging.capturing.valuetype.CaptureMode;
import com.dermalog.imaging.capturing.valuetype.DeviceIdentity;


public class DemoApplet extends JApplet implements OnImageListener, OnDetectListener {


	private static final long serialVersionUID = -1612559731329666158L;

	private JLabel _StatusLabel;
	private JButton _StartButton;
	private JLabel _PreviewLabel;
	private JLabel _DetectLabel;
	private ImagePanel _PreviewImagePanel;
	private ImagePanel _DetectImagePanel;

	private Device _DermalogScanner;

   
	
	
	// Called when this applet is loaded into the browser.
	@Override
	public void init() {
	    //Execute a job on the event-dispatching thread:
	    //creating this applet's GUI.
	    try {
	        javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
	            public void run() {
                	createGUI();
	            }
	        });
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	private void createGUI() {
		// gridbag layout constraints object
		GridBagConstraints layout;
		
		// main panel
		JPanel mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setLayout(new GridBagLayout());
				
		// preview label
		_PreviewLabel = new JLabel("Preview Image");
		layout = DemoApplet.createLayoutOptions(0, 0, 0, 0, 5);
		layout.fill = GridBagConstraints.CENTER;
		layout.gridwidth = 2;
		mainPanel.add(_PreviewLabel, layout);
		
		// preview image panel
		_PreviewImagePanel = new ImagePanel();
		layout = DemoApplet.createLayoutOptions(0, 1, 1, 5, 0);
		layout.gridwidth = 2;
		mainPanel.add(_PreviewImagePanel, layout);
			
		// detect label
		_DetectLabel = new JLabel("Detect Image");
		layout = DemoApplet.createLayoutOptions(2, 0, 0, 0, 5);
		layout.fill = GridBagConstraints.CENTER;
		layout.gridwidth = 2;
		mainPanel.add(_DetectLabel, layout);
		
		// detect image panel
		_DetectImagePanel = new ImagePanel();
		layout = DemoApplet.createLayoutOptions(2, 1, 1, 5, 0);
		layout.gridwidth = 2;
		mainPanel.add(_DetectImagePanel, layout);
		
		// status label
		_StatusLabel = new JLabel("Press Start");
		_StatusLabel.setFont(new Font("", Font.PLAIN, 20));
		layout = DemoApplet.createLayoutOptions(0, 2, 0, 0, 5);
		layout.gridwidth = 2;
		mainPanel.add(_StatusLabel, layout);
		
		// start button
		_StartButton = new JButton("Start Capture");
		layout = DemoApplet.createLayoutOptions(2, 2, 0, 0, 5);
		layout.gridwidth = 2;
		mainPanel.add(_StartButton, layout);
		_StartButton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent arg0) {
				startButton_Click();
			}
		});
		
		this.add(mainPanel);
	}
	
	public static GridBagConstraints createLayoutOptions(int gridX, int gridY, int weightX, int weightY, int border) {
		GridBagConstraints layout = new GridBagConstraints();
		layout.gridx = gridX;
		layout.gridy = gridY;
		layout.fill = GridBagConstraints.BOTH;
		layout.weightx = weightX;
		layout.weighty = weightY;
		layout.insets = new Insets(border, border, border, border);
		return layout;
	}
	
	private void startButton_Click() {
		try {
			if (_DermalogScanner == null) {
				LoadDeviceDialog dialog = new LoadDeviceDialog();
				if (dialog.showDialog() == LoadDeviceDialog.DialogResult.OK) {
					_StatusLabel.setText("Loading Device ...");
					initDermalogScanner(dialog._SelectedDeviceIdentity, dialog._SelectedCaptureMode);
				}else{
					return;
				}
			} 
				
			if (_DermalogScanner.isCapturing()) {
				_DermalogScanner.stop();
				_StartButton.setText("Start Capture");
			} else {
				_DermalogScanner.start();
				_StartButton.setText("Stop Capture");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Could not start scanner.\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void initDermalogScanner(DeviceIdentity scanner, CaptureMode capmode) {
		try {
                   
			_DermalogScanner = DeviceManager.getDevice(DeviceIdentity.FG_ZF10);
			_DermalogScanner.setCaptureMode(capmode);
			
			if (scanner == DeviceIdentity.FG_LF10) {
				_StatusLabel.setText(_DermalogScanner.getSerial());
				
			} else if (scanner == DeviceIdentity.FG_ZF10) {
				DeviceInfo[] info = _DermalogScanner.getDeviceInformations();
				_StatusLabel.setText("ZF10 " + info[0].getName());
				
			} else if (scanner == DeviceIdentity.FG_ZF2) {
				DeviceInfo[] info = _DermalogScanner.getDeviceInformations();
				_StatusLabel.setText(info[0].getName());
			}
			
			_DermalogScanner.addOnImageListener(this);
			_DermalogScanner.addOnDetectListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Called when this applet is unloaded.
	@Override
	public void stop() {
		try {
			_DermalogScanner.stop();
			_DermalogScanner.dispose();
						
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	public void onImage(Device arg0, int arg1, DermalogImage arg2) throws ListenerException {
		_PreviewImagePanel.setFingerprint(arg2.getImage());
	}

	public void onDetect(Device arg0, int arg1, DermalogImage arg2) {
		_DetectImagePanel.setFingerprint(arg2.getImage());
	}

}
