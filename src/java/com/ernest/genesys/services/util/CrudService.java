/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.services.util;

import com.stately.modules.jpa2.CrudController;
import com.stately.modules.jpa2.Enviroment;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Edwin
 */
@Stateless
public class CrudService extends CrudController implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private static final Logger LOGGER = Logger.getLogger(CrudService.class.getName());

    @PostConstruct
    private void init() {
        setEm(em);
        setEnviroment(Enviroment.JAVA_EE);

        System.out.println("new crud service created ... " + toString());
    }

    public int runQry(String qry) {
        int affectedResult = em.createQuery(qry).executeUpdate();
        String msg = affectedResult + " Updates on runing : " + qry;
        LOGGER.info(msg);
        return affectedResult;
    }

}
