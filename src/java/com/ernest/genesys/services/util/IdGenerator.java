/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.services.util;

import com.ernest.genesys.entity.registration.Applicant;
import com.ernest.genesys.entity.system.DvlaCenter;
import com.ernest.genesys.entity.system.LicenseCategory;
import com.google.common.base.Strings;
import com.stately.modules.jpa2.UniqueEntityModel;
import com.stately.modules.jpa2.UniqueEntityModel2;
import java.io.Serializable;
import java.util.UUID;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.slf4j.LoggerFactory;

/**
 *
 * @author IconU1
 */
@Stateless
public class IdGenerator implements Serializable{

     private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(IdGenerator.class);
    private static final Logger logger = Logger.getLogger(IdGenerator.class.getName());
    
      public void uniqueEntity(UniqueEntityModel entityModel) {
        if (!Strings.isNullOrEmpty(entityModel.getId())) {
            return;
        }
        entityModel.setId(generateId());
    }

    public void uniqueEntity2(UniqueEntityModel2 entityModel) {
        if (!Strings.isNullOrEmpty(entityModel.getId())) {
            return;
        }
        entityModel.setId(generateId());
    }
    
    public void dvlaCenterId(DvlaCenter dvlaCenter)
    {
        if (!Strings.isNullOrEmpty(dvlaCenter.getId())) {
            return;
        }
        dvlaCenter.setId(dvlaCenter.getCenterCode());
    }
    public void licenseCategoryId(LicenseCategory dvlaCenter)
    {
        if (!Strings.isNullOrEmpty(dvlaCenter.getId())) {
            return;
        }
        dvlaCenter.setId(dvlaCenter.getLicense());
    }
    public void applicantId(Applicant applicant )
    {
        if (!Strings.isNullOrEmpty(applicant.getApplicantCode())) {
            return;
        }
        applicant.setApplicantCode(generateId());
    }
    
    public String generateId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
