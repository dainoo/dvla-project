/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.services.common;

import com.ernest.genesys.entity.system.DvlaCenter;
import com.ernest.genesys.services.util.CrudService;
import com.icsecurities.common.entities.Country;
import com.stately.modules.jpa2.CrudController;
import com.stately.modules.jpa2.Enviroment;
import com.stately.modules.jpa2.QryBuilder;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author IconU1
 */
@Stateless
public class CommonServices extends CrudController implements Serializable{
 @PersistenceContext
    private EntityManager em;

    private static final Logger LOGGER = Logger.getLogger(CrudService.class.getName());

    @PostConstruct
    private void init() {
        setEm(em);
        setEnviroment(Enviroment.JAVA_EE);

        System.out.println("new crud service created ... " + toString());
    }
    
    public List<DvlaCenter> getAllDvlaCenter()
    {
        QryBuilder qryBuilder = new QryBuilder(em, DvlaCenter.class);
        qryBuilder.addObjectParam("deleted", false);
        return qryBuilder.buildQry().getResultList();
        
    }
    
    public List<Country> countrysList()
    {
        QryBuilder builder = new QryBuilder(em, Country.class);
        return builder.buildQry().getResultList();
    }
   
}
