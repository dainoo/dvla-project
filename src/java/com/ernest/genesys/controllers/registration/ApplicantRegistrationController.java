/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.registration;

import com.ernest.genesys.FingerPrintLeftThumb;
import com.ernest.genesys.entity.registration.ApplicantInformation;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "applicantRegistration")
@SessionScoped
public class ApplicantRegistrationController implements Serializable {

    private ApplicantInformation applicantInformation = new ApplicantInformation();
    private List<ApplicantInformation> applicantInformationsList = new ArrayList<>();
    @Inject
    private CrudService crudService;
    @Inject
    private IdGenerator idGenerator;

    public ApplicantRegistrationController() {
    }

    public void saveApplicantInformation() {
        idGenerator.uniqueEntity2(applicantInformation);
        if (crudService.save(applicantInformation) != null) {
            Msg.successSave();
            applicantInformationsList = new ArrayList<>();
            applicantInformationsList.add(applicantInformation);

            applicantInformation = new ApplicantInformation();
        } else {
            Msg.failedSave();
        }
    }

    public void clear() {
        applicantInformation = new ApplicantInformation();
    }

    public void selectApplicant(ApplicantInformation information) {
        applicantInformation = information;
    }

    public void deleteApplicant(ApplicantInformation information) {
        if (crudService.delete(information, true)) {
            Msg.successDelete();
        } else {
            Msg.failedDelete();
        }
    }

    public ApplicantInformation getApplicantInformation() {
        return applicantInformation;
    }

    public void setApplicantInformation(ApplicantInformation applicantInformation) {
        this.applicantInformation = applicantInformation;
    }

    public List<ApplicantInformation> getApplicantInformationsList() {
        return applicantInformationsList;
    }

    public void setApplicantInformationsList(List<ApplicantInformation> applicantInformationsList) {
        this.applicantInformationsList = applicantInformationsList;
    }

}
