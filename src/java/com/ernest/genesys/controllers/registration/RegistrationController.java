/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.registration;

import com.ernest.genesys.common.util.GenesysEntityModelMethod;
import com.ernest.genesys.entity.registration.Applicant;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.jpa2.EntityModel2;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author IconU1
 */
@Named(value = "registrationController")
@RequestScoped
public class RegistrationController implements GenesysEntityModelMethod{

   private Applicant applicant = new Applicant();
   @Inject private CrudService crudService;
   @Inject private IdGenerator idGenerator ;
   private UploadedFile file;
    public RegistrationController() {
    }
    
    

   

    @Override
    public void saveMethod() {
        idGenerator.applicantId(applicant);
        if(crudService.saveEntity(applicant) !=null)
        {
            Msg.successSave();
            applicant = new Applicant();
        }
        else{
            Msg.failedSave();
        }
    }
    
     public void upload() {
        if(file != null) {
           
        }
    }

    @Override
    public void clearMethod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectMethod(EntityModel2 entityModel2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteMethod(EntityModel2 entityModel2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    
    
}
