package com.ernest.genesys.controllers;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.ernest.genesys.common.util.UserData;
import com.stately.common.utils.DateTimeUtils;
import com.stately.modules.jasperreporting.ReportOutputFileType;
import com.stately.modules.web.jsf.JsfUtil;
import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Edwin
 */
@Named(value = "userdata")
@SessionScoped
public class UserSession extends UserData implements Serializable {

    private static final String PAGE_BEAN_NAME = "userdata";

//    @Inject
//    private ReportGenerator reportGenerator;

    private boolean showBanner = true;
    private boolean hasUserLogin = false;

    private String username = null;
    private String userPassword;

    private Date dateUR = DateTimeUtils.getDate(2016, 1, 31);

    private boolean debugMode = true;

    private ReportOutputFileType reportOutputType = ReportOutputFileType.PDF;

    public UserSession() {

    }

    public void changeCurrentFund() {
       // System.out.println("fund UR : " + getFundUR());
        //reportGenerator.setFund(getFundUR());
    }

//    public void track(EntityModel account) {
//        if (getUserAccount() != null) {
//            account.setLastModifiedBy(getUserAccount().getId());
//        }
//
//    }
//
//    public void track2(EntityModel2 account) {
//        if (getUserAccount() != null) {
//            account.setLastModifiedBy(getUserAccount().getId());
//        }
//
//    }

//    public boolean hasModule(String moduleName) {
//        boolean hasModule = false;
//        for (AppModule appModule : getUserModulesList()) {
//            if (appModule.getModuleName().equalsIgnoreCase(moduleName)) {
//                hasModule = true;
//                break;
//            }
//        }
//
//        return hasModule;
//    }

    public static UserSession getMgedInstance() {
        UserSession data = (UserSession) JsfUtil.getManagedBean(PAGE_BEAN_NAME);

        if (data != null) {
            return data;
        }

        throw new RuntimeException("Unable to create your session");
    }

//    public boolean isSponsosrSelected() {
//        return getSponsorUR() != null;
//    }

    public boolean sponsorSelectedMsg() {
//        if (isSponsosrSelected() == false) {
//            String msg = "Please select Sponsor";
//            Messages.addGlobalError(msg);
//            return false;
//        }
        return true;
    }

//    public void audit(EntityModel entityModel) {
//        try {
//            entityModel.setLastModifiedBy(getUserAccount().getId());
//        } catch (Exception e) {
//        }
//
//    }
//
//    public void audit2(EntityModel2 entityModel) {
//        try {
//            entityModel.setLastModifiedBy(getUserAccount().getId());
//        } catch (Exception e) {
//        }

    //}

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isDebugMode() {
        return debugMode;
    }

    public boolean isHasUserLogin() {
        return hasUserLogin;
    }

    public void setHasUserLogin(boolean hasUserLogin) {
        this.hasUserLogin = hasUserLogin;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    public ReportOutputFileType getReportOutputType() {
        return reportOutputType;
    }

    public void setReportOutputType(ReportOutputFileType reportOutputType) {
        this.reportOutputType = reportOutputType;
    }

    public Date getDateUR() {
        return dateUR;
    }

    public void setDateUR(Date dateUR) {
        this.dateUR = dateUR;
    }

}
