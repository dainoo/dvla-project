/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.common;

import com.ernest.genesys.entity.system.DvlaCenter;
import com.ernest.genesys.services.common.CommonServices;
import com.icsecurities.common.entities.Country;
import com.stately.common.constants.Gender;
import com.stately.common.constants.IdType;
import com.stately.common.constants.Region;
import com.stately.common.constants.Relationship;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "commonOption")
@SessionScoped
public class CommonOption implements Serializable {

    @Inject private CommonServices commonServices;
    public CommonOption() {
    }
    
    public List<DvlaCenter> getDvlaCenterList()
    {
        return commonServices.getAllDvlaCenter();
    }
    
    public List<Gender> getGendersList()
    {
        return Gender.humanGender();
    }
    public List<Relationship> getRelationshipList()
    {
        return Arrays.asList(Relationship.values()) ;
    }
    public List<Region> getRegionList()
    {
        return Arrays.asList(Region.values()) ;
    }
    public List<IdType> getIdTypeList()
    {
        return Arrays.asList(IdType.values()) ;
    }
    public List<Country> getCountryList()
    {
        return commonServices.countrysList();
    }
    
}
