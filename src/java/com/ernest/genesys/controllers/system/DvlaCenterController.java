/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers.system;

import com.ernest.genesys.entity.system.DvlaCenter;
import com.ernest.genesys.services.util.CrudService;
import com.ernest.genesys.services.util.IdGenerator;
import com.stately.modules.web.jsf.Msg;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "dvlaCenterController")
@SessionScoped
public class DvlaCenterController implements Serializable {

    private DvlaCenter dvlaCenter = new DvlaCenter();
    private List<DvlaCenter> dvlaCentersList = new ArrayList<>();
    @Inject
    private IdGenerator idGenerator;
    @Inject
    private CrudService crudService;

    public DvlaCenterController() {
    }

    public void saveCenter() {
        idGenerator.dvlaCenterId(dvlaCenter);
        if (crudService.save(dvlaCenter) != null) {
            dvlaCentersList = new ArrayList<>();
            dvlaCentersList = crudService.findAll(DvlaCenter.class);
            Msg.successSave();
        } else {
            Msg.failedSave();
        }

    }

    public void clear() {
        dvlaCenter = new DvlaCenter();
        dvlaCentersList = crudService.findAll(DvlaCenter.class);
    }

    public void selectCenter(DvlaCenter center) {
        dvlaCenter = center;
    }

    public void deleteCenter(DvlaCenter center) {
        if (crudService.delete(center, false)) {
            Msg.successDelete();
            dvlaCentersList.remove(center);
        } else {
            Msg.failedDelete();
        }
    }

    public DvlaCenter getDvlaCenter() {
        return dvlaCenter;
    }

    public void setDvlaCenter(DvlaCenter dvlaCenter) {
        this.dvlaCenter = dvlaCenter;
    }

    public List<DvlaCenter> getDvlaCentersList() {
        dvlaCentersList = crudService.findAll(DvlaCenter.class);
        System.out.println("THE SIZE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+dvlaCentersList.size());
        return dvlaCentersList;
    }

    public void setDvlaCentersList(List<DvlaCenter> dvlaCentersList) {
        this.dvlaCentersList = dvlaCentersList;
    }

}
