/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.controllers;

import com.stately.modules.web.jsf.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author IconU1
 */
@Named(value = "loginController")
@RequestScoped
public class LoginController {

    /**
     * Creates a new instance of LoginController
     */
    private String username;
    private String password;
    
    @Inject
    private UserSession userSession;
    public LoginController() {
    }
    
    public void loginButtonAction()
    {
        if(username.equalsIgnoreCase("system"))
        {
           userSession.setSystemLogin(true); 
            userSession.setRegistrationLogin(false);  
        }
        else  if(username.equalsIgnoreCase("app")){
           userSession.setRegistrationLogin(true);  
           userSession.setSystemLogin(false);
           
        }
      userSession.setHasUserLogin(true);
     
    }
    
    public void logoutUser()
    {
        userSession.setHasUserLogin(false);
        JsfUtil.invalidateSession();
        JsfUtil.resetViewRoot();
    }
    
    public String registrater()
    {
      return "/secured/registration/masters/registration_form.xhtml";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
