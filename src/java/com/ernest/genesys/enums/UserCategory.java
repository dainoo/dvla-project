/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

/**
 *
 * @author Edwin
 */
public enum UserCategory {

//    SUPER_USER("Super User","super user"),
    SPONSOR("Sponsor Administrator", "sponsor"),
    AMC("Asset Management Company", "am"),
    CUSTODIAN("Custodian", "custodian"),
    TRUSTEE("Trustee", "trustee"),
    FAC("Fund Administration Company", "fac"),
    SYSTEM_PROVIDER("System Provider", "system");

    String userCategory;
    String path;

    private UserCategory(String userCategory, String path) {
        this.userCategory = userCategory;
        this.path = path;
    }

    @Override
    public String toString() {
        return userCategory;
    }

    public String getPath() {
        return path;
    }

    public static UserCategory[] fac;

    public static UserCategory[] sponsorCategories;
    public static UserCategory[] amcCategories;
    public static UserCategory[] sysProvider;
    public static UserCategory[] superusers;

    static {
        fac = new UserCategory[]{SPONSOR, TRUSTEE, AMC};

        sponsorCategories = new UserCategory[]{SPONSOR};

        amcCategories = new UserCategory[]{AMC};

        sysProvider = new UserCategory[]{TRUSTEE, AMC, SPONSOR};

        superusers = new UserCategory[]{};

    }

}
