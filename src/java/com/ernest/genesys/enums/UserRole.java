/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Edwin
 */
public enum UserRole implements MessageResolvable {
    GUEST("Guest", "guest"),
    MEMBER("Member", "member"),
    ADMIN("Admin", "admin");

    String label;
    String code;

    private UserRole(String label, String code) {
        this.label = label;
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }

}
