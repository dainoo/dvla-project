/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Edwin
 */
public enum RateCategory implements MessageResolvable {
    VEHICLE("Vehicle", "vehicle"),
    DRIVER("Driver", "driver");
    

    String label;
    String code;

    private RateCategory(String label, String code) {
        this.label = label;
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }

}
