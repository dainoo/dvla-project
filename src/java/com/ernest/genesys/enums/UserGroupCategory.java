/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Edwin
 */
public enum UserGroupCategory implements MessageResolvable {
    ADMINISTRATIVE_GROUP("Administrator Group");

    String lable;

    private UserGroupCategory(String lable) {
        this.lable = lable;
    }

    @Override
    public String getCode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getLabel() {
        return lable;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

}
