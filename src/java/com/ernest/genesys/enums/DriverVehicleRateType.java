/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.enums;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Edwin
 */
public enum DriverVehicleRateType implements MessageResolvable {
    FIXED("Fixed", "fixed"),
    VARIABLE("Variable", "variable");
    

    String label;
    String code;

    private DriverVehicleRateType(String label, String code) {
        this.label = label;
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }

}
