package com.ernest.genesys.common.util;


import com.ernest.genesys.enums.UserCategory;
import com.stately.common.BasicUserData;
import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Edwin
 */
public class UserData extends BasicUserData implements Serializable {

    public static final String FILES_FOLDER = "fundadminfiles";
    private static final String UNITHOLDER_IMAGE_FOLDER = "unitholders-images";
    public static String DOCUMENT_FOLDER = "docs";
    public static String INSTITUTION_LOGO_FOLDER = "institutions-logo";
    public static final String SUBSCRIPTION_DATA = "subscription-datafiles";

    public static final String APP_ROOT = System.getProperty("com.sun.aas.instanceRoot")
            + File.separator + "docroot"
            + File.separator + FILES_FOLDER + File.separator;

    private String FILE_BASE_URI;
    private String uploadedDocsURI;
    private String uploadDocFolder;
    private String institutionsLogoFolder;

//    private UserAccount userAccount = null;
//    private Unitholder unitholderUR = null;

    private UserCategory userCategory = null;
   // private DataManagementRole dataManagementRole = null;

    private String documentFullPathBase = "";
    private String documentFullPathEscapted = "";
    private String serverRoootImagePath = "";

    //images
    private String unitHolderImagesBaseURL;
    private String unitHolderImageBaseFolder;
    private String institutionLogoBaseFolder;

//    private Fund fundUR;
//    private Sponsor sponsorUR;
//    private ServiceProviderRole stakeholderAssumedRole;

    private String institutionLogo = "";

    private String computerName;

    private String subscriptionDataFolder = "";

    private boolean showBanner = true;

    private String institutionId = null;

    private boolean userIsAmc;
    private boolean userIsAuthoriser;
    private boolean userIsTrustee;

    private boolean userIsSystemProvider;

//    private List<AppModule> userModulesList = new LinkedList<>();
//
//    private ServiceProvider stakeholderUR;

    private String amcName;

    private boolean amLogin;
    private boolean registrationLogin;
    private boolean trusteeLogin;
    private boolean systemLogin;
    private boolean superUser;

    public UserData() {
        try {
            computerName = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(UserData.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

//    public FundServiceProvider fund(Fund fund) {
//        FundServiceProvider fundStakeholder = new FundServiceProvider();
//        fundStakeholder.setFund(fund);
//
//        return fundStakeholder;
//    }
//
//    public FundServiceProvider stakeholder(ServiceProvider stakeholder) {
//        FundServiceProvider fundStakeholder = new FundServiceProvider();
//        fundStakeholder.setStakeholder(stakeholder);
//
//        return fundStakeholder;
//    }

    public void init() {

        try {
            FILE_BASE_URI = "http://" + computerName + "/" + FILES_FOLDER + "/";

            uploadDocFolder = APP_ROOT + DOCUMENT_FOLDER + File.separator;
            uploadedDocsURI = FILE_BASE_URI + DOCUMENT_FOLDER + "/";

            unitHolderImagesBaseURL = FILE_BASE_URI + institutionId + "/" + UNITHOLDER_IMAGE_FOLDER + "/";
            unitHolderImageBaseFolder = APP_ROOT + institutionId + File.separator + UNITHOLDER_IMAGE_FOLDER;
            institutionLogoBaseFolder = APP_ROOT + INSTITUTION_LOGO_FOLDER + File.separator;

            subscriptionDataFolder = APP_ROOT + SUBSCRIPTION_DATA;

            System.out.println("APP_ROOT = " + APP_ROOT);

//        System.out.println("uploadedDocsURI : " + uploadedDocsURI);
//        System.out.println("uploadDocFolder : " + uploadDocFolder);
//        System.out.println("institutionLogo path: " + institutionLogoBaseFolder);
//        System.out.println("Subscription Data Files: " + subscriptionDataFolder);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public String sponsorURImageFolder() {
//        String sponsorImageFolder = unitHolderImageBaseFolder
//                + File.separator
//                + sponsorUR.getSponsorRefNo();
//
//        return sponsorImageFolder;
//    }

    public String defFullId(String fundId) {
        if (fundId.contains(institutionId + "#")) {
            return fundId;
        } else {
            return institutionId + "#" + fundId;
        }
    }

    public UserCategory getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(UserCategory userCategory) {
        this.userCategory = userCategory;
    }

//    public String getUnitHolderImagePath(Unitholder unitHolder) {
//        String path = unitHolderImageBaseFolder
//                + File.separator
//                + unitHolder.getSponsor().getSponsorRefNo()
//                + File.separator
//                + unitHolder.getUnitholderRefNo() + ".jpg";
//
//        File file = new File(path);
//
//        if (file.exists()) {
//            return path;
//        }
//
//        return null;
//    }

    public String getUploadedDocsURI() {
        return uploadedDocsURI;
    }

    public String getUploadDocFolder() {
        return uploadDocFolder;
    }

    public String getInstitutionLogo() {
        return institutionLogo;
    }

    public void setInstitutionLogo(String institutionLogo) {
        this.institutionLogo = institutionLogo;
    }

    public String getServerRoootImagePath() {
        return serverRoootImagePath;
    }

    public void setServerRoootImagePath(String serverRoootImagePath) {
        this.serverRoootImagePath = serverRoootImagePath;
    }

//    public UserAccount getUserAccount() {
//        return userAccount;
//    }
//
//    public Unitholder getUnitholderUR() {
//        return unitholderUR;
//    }
//
//    public void setUnitholderUR(Unitholder unitholderUR) {
//        this.unitholderUR = unitholderUR;
//    }
//
//    public void setUser(UserAccount currentUserAccount) 
//    {
//        if (currentUserAccount == null) {
//            return;
//        }
//
//        this.userAccount = currentUserAccount;
//        if (currentUserAccount.getUsername().startsWith(kConstants.SYSTEM_PROVIDER_UA) == true) 
//        {
//            systemLogin = true;
//        }
//        if (currentUserAccount.getStakeholder().getServiceProviderRole() == ServiceProviderRole.REGULATOR) 
//        {
//            systemLogin = true;
//        }
//
//        System.out.println(currentUserAccount.toString2());
//
//        setHasUserLogin(true);
//        setFullName(currentUserAccount.getUserFullName());
//
//        setUserId(currentUserAccount.getId());
//
//        userCategory = currentUserAccount.getUserCategory();
//        dataManagementRole = currentUserAccount.getDataManagementRole();
//
//        if (currentUserAccount.getStakeholder() != null) {
//            institutionLogo = currentUserAccount.getStakeholder().getLogo();
//        }
//
//        if (userAccount.getDataManagementRole() == DataManagementRole.SUPER_USER) {
//            if (userAccount.getUsername().contains("win")) {
//                superUser = true;
//            }
//            if (userAccount.getUsername().contains("araba")) {
//                superUser = true;
//            }
//
//        }
//
//        if (userCategory != null) {
//
//            if (userCategory == UserCategory.AMC) {
//                amLogin = true;
//
//            } else if (userCategory == UserCategory.TRUSTEE) {
//                userIsTrustee = true;
//                trusteeLogin = true;
//
//                System.out.println("institution logo is " + institutionLogo);
//            } else if (userCategory == UserCategory.SYSTEM_PROVIDER) {
//                systemLogin = true;
//
//                System.out.println("institution logo is " + institutionLogo);
//            }
//
//        }
//
//        if (userAccount.getDataManagementRole() != null) {
//            userIsAuthoriser = userAccount.getDataManagementRole() == DataManagementRole.AUTHORISER ? true : false;
//        }
//
//        init();
//
//    }

    public void loadSettings() {
//        setSettings(SettingsLoader.loadSettings());
    }

//    public void setSettings(List<Setting> settings) {
//
//    }

    public String getInstitutionLogoBaseFolder() {
        return institutionLogoBaseFolder;
    }

    public void setInstitutionLogoBaseFolder(String institutionLogoBaseFolder) {
        this.institutionLogoBaseFolder = institutionLogoBaseFolder;
    }

    public String getUnitHolderImagesBaseURL() {
        return unitHolderImagesBaseURL;
    }

    public void setUnitHolderImagesBaseURL(String unitHolderImagesBaseURL) {
        this.unitHolderImagesBaseURL = unitHolderImagesBaseURL;
    }

    public boolean isShowBanner() {
        return showBanner;
    }

    public void setShowBanner(boolean showBanner) {
        this.showBanner = showBanner;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public boolean isRegistrationLogin() {
        return registrationLogin;
    }

    public void setRegistrationLogin(boolean registrationLogin) {
        this.registrationLogin = registrationLogin;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isUserIsSystemProvider() {
        return userIsSystemProvider;
    }

    public void setUserIsSystemProvider(boolean userIsSystemProvider) {
        this.userIsSystemProvider = userIsSystemProvider;
    }

    public boolean isUserIsTrustee() {
        return userIsTrustee;
    }

    public void setUserIsTrustee(boolean userIsTrustee) {
        this.userIsTrustee = userIsTrustee;
    }

    public String getSubscriptionDataFolder() {
        return subscriptionDataFolder;
    }

    public void setSubscriptionDataFolder(String subscriptionDataFolder) {
        this.subscriptionDataFolder = subscriptionDataFolder;
    }

//    public DataManagementRole getDataManagementRole() {
//        return dataManagementRole;
//    }
//
//    public void setDataManagementRole(DataManagementRole dataManagementRole) {
//        this.dataManagementRole = dataManagementRole;
//    }

    public boolean isUserIsAmc() {
        return userIsAmc;
    }

    public void setUserIsAmc(boolean userIsAmc) {
        this.userIsAmc = userIsAmc;
    }

    public boolean isUserIsAuthoriser() {
        return userIsAuthoriser;
    }

    public void setUserIsAuthoriser(boolean userIsAuthoriser) {
        this.userIsAuthoriser = userIsAuthoriser;
    }

    public String getAmcName() {
        return amcName;
    }

    public void setAmcName(String amcName) {
        this.amcName = amcName;
    }

//    public ServiceProvider getUserInstitution() {
//        if (userAccount == null) {
//            return null;
//        }
//
//        return userAccount.getStakeholder();
//
//    }

   

    public boolean isAmLogin() {
        return amLogin;
    }

    public void setAmLogin(boolean amLogin) {
        this.amLogin = amLogin;
    }

    public boolean isTrusteeLogin() {
        return trusteeLogin;
    }

    public void setTrusteeLogin(boolean trusteeLogin) {
        this.trusteeLogin = trusteeLogin;
    }

    public boolean isSystemLogin() {
        return systemLogin;
    }

    public void setSystemLogin(boolean systemLogin) {
        this.systemLogin = systemLogin;
    }

//    public FundServiceProvider getFundStakeholderUR()
//    {
//        return fundStakeholderUR;
//    }
//
//    public void setFundStakeholderUR(FundServiceProvider fundStakeholderUR)
//    {
//        this.fundStakeholderUR = fundStakeholderUR;
//        if(fundStakeholderUR != null)
//        {
//            setStakeholderUR(fundStakeholderUR.getStakeholder());
//        }
//    }
    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

}
