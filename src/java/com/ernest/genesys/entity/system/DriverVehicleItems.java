/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system;

import com.ernest.genesys.enums.DriverVehicleRateType;
import com.stately.modules.jpa2.EntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "driver_vehicle_items")
public class DriverVehicleItems extends EntityModel2 implements Serializable{
    
    @Id
    @Column(name = "item_code")
    private String itemCode;
    
    @Column(name = "item_name")
    private String itemName;
    
    @Column(name = "driver_vehicle_rate_type")
    @Enumerated(EnumType.STRING)
    private DriverVehicleRateType driverVehicleRateType ;
    
    @JoinColumn(name = "dvla_accounts")
    private DvlaAccounts dvlaAccounts;
    
   

    public DriverVehicleItems() {
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public DriverVehicleRateType getDriverVehicleRateType() {
        return driverVehicleRateType;
    }

    public void setDriverVehicleRateType(DriverVehicleRateType driverVehicleRateType) {
        this.driverVehicleRateType = driverVehicleRateType;
    }

    public DvlaAccounts getDvlaAccounts() {
        return dvlaAccounts;
    }

    public void setDvlaAccounts(DvlaAccounts dvlaAccounts) {
        this.dvlaAccounts = dvlaAccounts;
    }

   

   
    
    
}
