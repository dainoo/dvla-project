/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.system;

import com.stately.modules.jpa2.EntityModel2;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "driver_vehicle_services")
public class DriverVehicleServices extends EntityModel2 implements Serializable{
    
    @Id
    @Column(name = "service_code")
    private String serviceCode;
    
    @Column(name = "service_name")
    private String serviceName;
    
    @Column(name = "service_title")
    private String serviceTitle;
    
   

    public DriverVehicleServices() {
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

   
    
    
}
