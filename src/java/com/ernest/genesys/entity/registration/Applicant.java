/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.genesys.entity.registration;

import com.icsecurities.common.entities.Country;
import com.stately.common.constants.Gender;
import com.stately.common.constants.IdType;
import com.stately.common.constants.Region;
import com.stately.common.constants.Relationship;
import com.stately.modules.jpa2.EntityModel2;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author IconU1
 */
@Entity
@Table(name = "applicant_personal_information")
public class Applicant extends EntityModel2 implements Serializable{
    
    @Id
    @Column(name = "applicant_code")
    private String applicantCode;
    
    @Column(name = "surname")
    private String surname;
    
    @Column(name = "othername")
    private String othername;
    
    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "email_address")
    private String emailAddress;
    
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    
    @Column(name = "telephone")
    private String telephone;
    
    @Column(name = "residential")
    private String residential;
    
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;
    
    @Column(name = "region")
    @Enumerated(EnumType.STRING)
    private Region region ;
    
    @JoinColumn(name = "nationality")
    private Country nationality ;
    
    @Column(name = "picture_url")
    private String pictureURL;
    
    @Column(name = "signature_url")
    private String signatureURL;  
    
    @Column(name = "id_type_no")
    private String idTypeNo;
    
    @Column(name = "place_of_birth")
    private String placeOfBirth;  
    
    @Column(name = "id_type")
    private IdType idType ;  
    
    @Column(name = "contact_person_surname")
    private String contactPersonSurname ;  
    
    @Column(name = "contact_person_othername")
    private String contactPersonOthername ;  
    
    @Column(name = "contact_person_telephone")
    private String contactPersonTelephone ;  
    
    @Column(name = "relationship")
     @Enumerated(EnumType.STRING)
    private Relationship relationship  ;  

    public Applicant() {
    }

    public String getApplicantCode() {
        return applicantCode;
    }

    public void setApplicantCode(String applicantCode) {
        this.applicantCode = applicantCode;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getResidential() {
        return residential;
    }

    public void setResidential(String residential) {
        this.residential = residential;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Country getNationality() {
        return nationality;
    }

    public void setNationality(Country nationality) {
        this.nationality = nationality;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getSignatureURL() {
        return signatureURL;
    }

    public void setSignatureURL(String signatureURL) {
        this.signatureURL = signatureURL;
    }

    public String getIdTypeNo() {
        return idTypeNo;
    }

    public void setIdTypeNo(String idTypeNo) {
        this.idTypeNo = idTypeNo;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public String getContactPersonSurname() {
        return contactPersonSurname;
    }

    public void setContactPersonSurname(String contactPersonSurname) {
        this.contactPersonSurname = contactPersonSurname;
    }

    public String getContactPersonOthername() {
        return contactPersonOthername;
    }

    public void setContactPersonOthername(String contactPersonOthername) {
        this.contactPersonOthername = contactPersonOthername;
    }

    public String getContactPersonTelephone() {
        return contactPersonTelephone;
    }

    public void setContactPersonTelephone(String contactPersonTelephone) {
        this.contactPersonTelephone = contactPersonTelephone;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public void setRelationship(Relationship relationship) {
        this.relationship = relationship;
    }
    
    
    
    
    
}
